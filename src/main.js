import Vue from 'vue'
import './components'
import './plugins'
import { sync } from 'vuex-router-sync'
import App from './App'
import router from '@/router'
import store from '@/store'
import axios from "axios"
import VueCookies from 'vue-cookies'
import Loader91 from "loader91";
Vue.use(require('vue-moment'));
import Toast from "vue2-toast";
Vue.prototype.axios = axios;
Vue.use(VueCookies);
Vue.use(Loader91);
Vue.use(Toast, {
  defaultType: "center",
  duration: 2000,
  wordWrap: true,
  width: "400px"
});

sync(store, router)
Vue.config.productionTip = false

window.serverUrl = '/'
if (window.location.hostname === "localhost")
{window.serverUrl = "http://localhost:5000/";}

new Vue({  
  router,
  store,
  render: h => h(App)
}).$mount('#app')
