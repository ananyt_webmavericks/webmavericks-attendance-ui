import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import Dashboard from '@/views/Dashboard.vue'
import Login from '@/views/Login.vue'
import Employees from '@/views/Employees.vue'
import Employers from '@/views/Employers.vue'
import Jobs from '@/views/Jobs.vue'
import EmployerForm from '@/views/EmployerForm.vue'
import EmployeeForm from '@/views/EmployeeForm.vue'
import AssignJobs from '@/views/AssignJobs.vue'
import Application from '@/views/Application.vue'
import JobsById from '@/views/JobsById.vue'
import Settings from '@/views/Settings.vue'
import Documents from '@/views/Documents.vue'

import ReportByJobId from '@/views/ReportByJobId.vue'
import ReviewByJobId from '@/views/ReviewByJobId.vue'

import Report from '@/views/Report.vue'
import Review from '@/views/Review.vue'


Vue.use(Router)
Vue.use(Meta)

const router = new Router({
  routes: [
    {
      path: '',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true,
        title:'Dashboard'
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        requiresAuth: false,
        title:'Login'
      }
    },
    {
      path: '/employees',
      name: 'Employees',
      component: Employees,
      meta: {
        requiresAuth: true,
        title:'Employees'
      }
    },
    {
      path: '/edit-employee/:id',
      name: 'EmployeeForm',
      component: EmployeeForm,
      meta: {
        name: "edit",
        requiresAuth: true,
        title:'Edit Employee'
      }
    },
    {
      path: '/employers',
      name: 'Employers',
      component: Employers,
      meta: {
        requiresAuth: true,
        title:'Employers'
      }
    },
    {
      path: '/documents',
      name: 'Documents',
      component: Documents,
      meta: {
        requiresAuth: true,
        title:'Documents'
      }
    },
    {
      path: '/add-employer',
      name: 'EmployerForm',
      component: EmployerForm,
      meta: {
        name: "add",
        requiresAuth: true,
        title:'Add Employer'
      }
    },
    {
      path: '/edit-employer/:id',
      name: 'EmployerForm',
      component: EmployerForm,
      meta: {
        name: "edit",
        requiresAuth: true,
        title:'Edit Employer'
      }
    },
   
    {
      path: '/assign-jobs',
      name: 'AssignJobs',
      component: AssignJobs,
      meta: {
        requiresAuth: true,
        title:'Assign Jobs'
      }
    },
    {
      path: '/jobs',
      name: 'Jobs',
      component: Jobs,
      meta: {
        requiresAuth: true,
        title:'Jobs'
      }
    },
    {
      path: '/jobs/:id/application',
      name: 'JobsById',
      component: JobsById,
      meta: {
        requiresAuth: true,
        title:'Job Details'
      }
    },
    {
      path: '/application',
      name: 'Application',
      component: Application,
      meta: {
        requiresAuth: true,
        title:'Application'
      }
    },
    {
      path: '/review',
      name: 'Review',
      component: Review,
      meta: {
        requiresAuth: true,
        title:'Reviews'
      }
    },
    {
      path: '/report',
      name: 'Report',
      component: Report,
      meta: {
        requiresAuth: true,
        title:'Reports'
      }
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        requiresAuth: true,
        title:'Settings'
      }
    },
    {
      path: '/jobs/:id/report',
      name: 'ReportByJobId',
      component: ReportByJobId,
      meta: {
        requiresAuth: true,
        title:'Job Reports'
      }
    },
    {
      path: '/jobs/:id/review',
      name: 'ReviewByJobId',
      component: ReviewByJobId,
      meta: {
        requiresAuth: true,
        title:'Job reviews'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let user = JSON.parse(localStorage.getItem('user'))
    if (user !== null && Object.keys(user).length > 0) {
      next()
    } else {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    }
  } else {
    next()
  }
})

export default router
