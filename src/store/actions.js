// https://vuex.vuejs.org/en/actions.html

export default {
  setsavedata: ({ commit, state }, payload) => {
      commit('savedata', payload)
      return state.savedata
  },
}
