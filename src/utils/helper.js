export const set = property => (state, payload) => (state[property] = payload)

export const toggle = property => state => (state[property] = !state[property])

export const toProperCase = (str) => {
    if(str !==undefined && str!=='') {
      return str.toLowerCase().replace(/^(.)|\s(.)/g, 
              function($1) { return $1.toUpperCase(); });
    } else {
      return str
    }
  }